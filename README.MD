# TASK MANAGER

## DEVELOPER INFO

**NAME**: Anastasiya Kharitonova

**EMAIL**: akharitonova@t1-consulting.ru

**EMAIL**: nasbred@yandex.ru

## SOFTWARE

**JAVA**: OPENJDK 1.8

**OS**: Windows 10

## HARDWARE

**CPU**: i5

**RAM** :16GB

**SSD**: 512GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
